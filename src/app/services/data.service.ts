import { Injectable } from '@angular/core';
import { CallTracker } from 'assert';
import { Carta } from '../pages/tab2/tab2.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  BASE_URL: string='http://albert-rodriguez-7e3.alwaysdata.net/apisanpina';
  productos: Carta[] = [];
  singleProducto: Carta;

  constructor() { }

  async getCarta(){

    this.productos=[];
    this.productos= await (await fetch(`${this.BASE_URL}/getCarta`)).json();

    console.log(this.BASE_URL+"/getCarta");
    //console.log(this.productos);
    return this.productos;
  }

  


}
