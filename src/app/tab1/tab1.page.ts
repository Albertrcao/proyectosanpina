import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private qr:BarcodeScanner, private data:DataService) {}

  scan(){
    this.qr.scan().then(res=>{
      console.log(res)
    }).catch(err=>{
      console.log(err)
      //this.data.guardarRegistro('QRCode','https://ionicframework.com/')
       this.data.guardarRegistro('QRCode','geo:41.4508647,2.1908592')
    })

  }

}
