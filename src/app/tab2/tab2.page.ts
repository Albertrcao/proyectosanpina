import { Component } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  guardados=[]
  constructor(private data:DataService) {}

  async ngOnInit(){
    this.guardados=await this.data.getGuardados()

  }
  async ionViewWillEnter(){
      this.ngOnInit()
  }

  enviarCorreo(){
    this.data.enviarCorreo();
  }

  abrirRegistro(registro){
    this.data.abrirRegistro(registro)
  }

}
