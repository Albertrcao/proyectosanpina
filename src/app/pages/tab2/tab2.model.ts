export interface Carta{
    id: number;
    nombre?: string;
    categoria?: string;
    precio?: number; //no sabemos bien si es number o string
    cantidad?: number;
    imagen?: string;
}
export interface ProductoFiltrado{
    title: string;
    image:string;
}