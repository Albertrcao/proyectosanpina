import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import{Router} from '@angular/router';
import { Carta, ProductoFiltrado } from './tab2.model';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  productos: Carta[] = [];
  singleProducto: Carta;
  /*productoFiltrado: ProductoFiltrado[] = [
    {
      title: 'Todos los productos',
      image: 'https://img2.freepng.es/20190923/owp/transparent-french-fries-etiquettespancartestubesscrap5d8e11f3479ff7.9060318715695917952934.jpg', 
    }
  ];*/



  constructor(private data: DataService, private router: Router) {}
  
  ngOnInit(){
    this.cargarCarta();
     console.log('OnInit');
  }

  ionViewWillEnter(){
    this.cargarCarta();
    console.log('IonView');
  }

  async cargarCarta(){
    //(async ()=>{this.productos= await this.data.getCarta();})();
    this.productos= await this.data.getCarta();
    console.log(this.productos); 
  }

  }

